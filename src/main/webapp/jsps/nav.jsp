<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<style type="text/css">
html, body, div, span, object, iframe,h1, h2, h3, h4, h5, h6, p, blockquote, pre,abbr, address, cite, code,del, dfn, em, img, ins, kbd, q, samp,small, strong, sub, sup, var,b, i,dl, dt, dd, ol, ul, li,fieldset, form, label, legend,table, caption, tbody, tfoot, thead, tr, th, td,article, aside, canvas, details, figcaption, figure, footer, header, hgroup, menu, nav, section, summary,time, mark, audio, video {margin: 0;padding: 0;}
fieldset, img {border: 0;}
/* remember to define focus styles! */
:focus {outline: 0;}
address, cite, code, dfn,em,i, strong, th, var, optgroup {font-style: normal;font-weight: normal;}
h1, h2, h3, h4, h5, h6 {font-size: 100%;font-weight: normal;}
img, a {border: none;text-decoration: none;}
a {cursor: pointer;}
abbr, acronym {border: 0;font-variant: normal;}
input, button, textarea,select, optgroup, option {font-family: inherit;font-size: inherit;font-style: inherit;font-weight: inherit;
}
code, kbd, samp, tt {font-size: 100%;}
/*@purpose To enable resizing for IE */
/*@branch For IE6-Win, IE7-Win */
input, button, textarea, select { *font-size: 100%;}
body {line-height: 1.5;font-family:"Microsoft YaHei", Helvetica, STHeiTi, Arial, sans-serif;font-size: 16px;color: #111;}
ol, ul {list-style: none;}
/* tables still need 'cellspacing="0"' in the markup */
table {border-collapse: collapse;border-spacing: 0;}
th {text-align: left;}
sup, sub {font-size: 100%;vertical-align: baseline;}
/* remember to highlight anchors and inserts somehow! */
:link, :visited , ins {text-decoration: none;}
blockquote, q {quotes: none;}
blockquote:before, blockquote:after,q:before, q:after {content: '';content: none;}
i,em {font-style: normal;}
.clearfix:after {visibility:hidden;display:block;font-size:0;content: " ";clear:both;height:0;}
.clearfix {*zoom:1;}
.moko-nav {
	height: 54px;
	background-color: white;
	border-bottom: #ccc solid 1px;
	padding-top: 16px;
	width: 100%;
	font-size: 14px;
}
.moko-nav a {
	display: inline-block;
	text-decoration: none;
	color: #666;
}
.moko-nav a:hover {color: #f09}
.dropdown-menu li a{color: #fff;}
.dropdown-menu li a:hover{color: #fff;}
.moko-nav .max-width {
    display:table;
	margin: 0 auto;
	height: 36px;
	line-height: 36px;
	position: relative;
}
#navmenu_company ul{ width:130px;}
.moko-nav .logo {
	float: left;
	margin-right: 2px;
	width: 198px;
	height: 36px;
	display: block;
	background: url('http://html.moko.cc/images/nav-logo-new.png') no-repeat;
	position: relative;
	zoom: 1;
}
.moko-nav .logo span {
	position: absolute;
	left: 160px;
	top: 2px;
	display: inline-block;
	width: 40px;
	height: 10px;
	background-position: 999px
}
.moko-nav .logo:hover span {background-position: 0 -106px}
.moko-nav .nav-pills li {float: left}

.moko-nav .nav-pills a {display:line-block;padding:0 30px;}
.moko_nav{ float: left; margin-left:44px;}
.moko-nav .split-line {
	display: inline-block;
	width: 1px;
	height: 28px;
	overflow: hidden;
	float: left;
	background: #ccc;
	margin-top:4px;
}
.moko-nav .search {
	border: #ccc solid 1px;
	background: #fff;
	height: 30px;
	float: left;
	overflow: hidden;
	zoom: 1
}
.moko-nav .search input {
	border: 0;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075)
}
.moko-nav .search .search-type {
	font-size: 12px;
	float: left;
	color: #999;
	margin: 0 2px 0 1px;
	cursor: pointer;
	width: 28px;
	height: 52px;
	display: inline-block;
	background-position: 0 -48px
}
.moko-nav .search .search-text {
	width: 100px;
	float: left;
	padding: 5px;
	height: 20px;
	line-height: 20px
}
.moko-nav .search .search-btn {
	background-position: right -48px;
	border: 0;
	float: left;
	height:24px;
	width:24px ;
	background: url('http://html.moko.cc/images/newnav/nav-serch.png') no-repeat;
	cursor: pointer;
}
.moko-nav .holder-user .search-type {margin-top: -28px}
.moko-nav .dropdown {position: relative;z-index: 5;text-align: center;}
.moko-nav .new_active {background-color: #f09;color:#fff;}
.moko-nav .new_active:hover {color:#fff;}
.moko-nav .dropdown .dropdown-toggle {
	width:130px;
	border: 0;
	padding: 0;
	vertical-align: top;
	*margin-top: -3px;
	_margin-top: -1px
}

.moko-nav .dropdown .dropdown-menu {background:url("http://html.moko.cc/images/nan_bg.png") repeat-y;display: none;position: absolute;left: 0;width: 110px;}
.moko-nav .dropdown .dropdown-menu li {float: none;margin: 0}
.moko-nav .dropdown .dropdown-menu li a {display: block;white-space: nowrap}
.moko-nav .dropdown .dropdown-menu li a:hover {background: #ff0066}
.moko-nav .login_right {float: left}
.moko-nav .login_right li {float: left;}
.reg{margin:0 30px;}
.moko-nav .login_right li .qq-login,.moko-nav .login_right li .sina-login {
	width: 63px;
	height: 24px;
	padding: 0;
	display: inline-block
}
.moko-nav .login_right li .qq-login {background-position: 0 -24px}
</style>
</head>
<body>
<nav class="moko-nav">
	<div class="max-width">
		<a href="http://www.moko.cc" hidefocus="true" class="logo"></a>
		<ul class="nav-pills moko_nav">
			<li class="dropdown">
				<a id="nav_post" href="http://www.moko.cc/moko/post/1.html" hidefocus="true" >精选</a>
			</li>
			<li class="split-line"></li>
			<li  class="dropdown">
				<a id="nav_mtb" href="http://www.moko.cc/focus.html">聚娇</a>
			</li>
			<li class="split-line"></li>
			<li  class="dropdown">
				<a id="nav_mtb" href="http://www.moko.cc/mtb.html" >人才库</a>
			</li>
			<li class="split-line"></li>
			<li  class="dropdown" style="">
				<a id="" href="http://www.moko.cc/dreamwork.html" >梦工场</a>
			</li>
			<li class="split-line"></li>
			<li id="navmenu_company" class="dropdown">
				<a id="nav_company" href="javascript:void(0)" hidefocus="true" class="dropdown-toggle">机遇 <span class="fs10">▼</span></a>
				<ul class="dropdown-menu">
					<li><a href="http://www.moko.cc/mtg/index.html" hidefocus="true">超级美女榜</a></li>
					<li><a href="http://www.moko.cc/job/list.html" hidefocus="true">工作机会</a></li>
					<li><a href="http://www.moko.cc/mokoevent/post/1.html" hidefocus="true">美空事件</a></li>
					<li><a href="http://www.moko.cc/dazhizuo" hidefocus="true">美空大制作</a></li>
					<li><a href="http://www.moko.cc/company/index.html" hidefocus="true">市场合作</a></li>
				</ul>
			</li>
		</ul>
		<div class="split-line"></div>
		<!-- 搜索框 begin
		<div class="search  holder-user "  style="height:24px; margin-top: 5px;">
			<form action="/catalog/searchUser.html" method="post" id="findFormNav" onsubmit="return jQuery1.enterBindClick('searchButtonNav');">
				<input type="button" id="searchButtonNav" class="search-btn" title="搜索"/>
				<input type="text" class="search-text" placeholder="" value="" name="findName" id="findNameNav" style="height:19px; width: 120px; padding: 3px;"  />
			</form>
			<input type="hidden" id="findUser" value="/catalog/searchUser.html"/>
			<input type="hidden" id="findPost" value="/catalog/searchPost.html"/>
		</div>
		搜索框 end -->
		<!-- 导航右侧 begin 
		<!-- 导航右侧 end -->
	</div>
</nav>
<script type="text/javascript">
$(function(){
	$('#nav_company,#navmenu_company ul').hover(function(){
		$('#navmenu_company ul').show();
		$('#nav_company').addClass('new_active');
	},function(){
		$('#navmenu_company ul').hide();
		$('#nav_company').removeClass('new_active');
	});
});

</script>
</body>
</html>