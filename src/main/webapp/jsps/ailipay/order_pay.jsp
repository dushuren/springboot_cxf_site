<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>订单支付</title>
<meta name="description" content="">
<meta name="keywords" content="">
<link href="../../css/modelShoot.css" rel="stylesheet">
<script src="../../js/jquery-2.1.4.min.js"></script>
<script>
$(function(){
	$("#button_dopay,#a_qiye_pay").one("click", function(){
		var payType = $("#select_paytype :radio:checked").val();
		location.href="/order/pay/"+payType;
	});
});
</script>
</head>
<body>

<jsp:include page="../nav.jsp"/>

<div class="order">
	<div class="order_nav order_nav02">
		<ul>
			<li>订单信息确认页</li>
			<li class="current">支付</li>
			<li>完成</li>
		</ul>
	</div>
	<div class="pay_intro pay_intro01">
		<div class="ph">订单提交成功，请您尽快付款！ 订单号：<span><font color="red"> 暂时未定，自行后台生成 </font></span></div>
		<p>请您在提交订单后24小时内完成支付，否则订单会自动取消。</p>
	</div>
	<div class="pay_intro pay_intro02">
		<div>
			订单类型：<span>支付订单</span>
		</div>
		<div>联系人：<span>hy~~~  15117996637</span>收货地址：<span> 海淀 </span></div>
		<div>购买时间：<span>-----</span></div>   
	</div>
	<div class="pay_style">
		<h3>选择支付方式</h3>
		<div class="pay_style_list">
			<dl class="clearfix" id="select_paytype">
				<dt>
					<h5>支付平台</h5>
					<p>手机或大额支付推荐使用支付宝</p>
					<p>快捷支付</p>
				</dt>
				<dd class="alipay online">
                	<input id="alipay" name="radio" type="radio" value="2" checked>
					<label for="alipay"><img src="http://static1qn.moko.cc/static/mbbsite/images/pay.png" alt=""></label>
                </dd>
				<dd class="wechat online">
                	<input id="wechat" name="radio" type="radio" value="1"/>
                	<img src="http://static1qn.moko.cc/static/mbbsite/images/wechat01.png" alt="">
					<label for="wechat">微信支付</label>
				</dd>
				<!-- 
				<dd class="yibao online">
                	<input id="online_bank" name="radio" type="radio" value="3">
					<label for="online_bank">网上银行</label>
				</dd> -->
				<dd class="transfer">
                	<input id="transfer" name="radio" type="radio" value="9">
					<label for="transfer">企业转账</label>
				</dd>
			</dl>
			
		</div>
		<div class="pay_type">
			<!-- <p><span class="ph">*</span> 如果需要发票请在下单前联系美空客服人员 010 - 57027637</p> -->
			<div class="pay_ok">
				<p>应付：<span class="ph">0.01元</span></p>
				<a class="ph_bg" id="button_dopay">确认支付</a>
			</div>
			<div class="pay_qyzz">
				<p class="ph">企业汇款（到账周期为1-2个工作日）</p>
				<h5>注意事项：汇款时需要注意以下信息，请牢记！</h5>
				<p>1. 您的汇款识别码为:<span class="ph">${orderInfo.ordernum}</span>，线下公司转账需将此汇款识别码填写至电汇凭证的【汇款用途】栏内，汇款识码组成：moko+订单号。企业网银转账请将汇款识别码填写到附言、摘要备注等栏内。</p>
				<p>2. 线下公司转账汇款时备注汇款识别码，可确保订单及时核销，请务必填写正确，勿私自增加其他文字说明。</p>
				<p>3. 线下公司转账订单，一个识别码对应一个订单和相应的金额，请勿多转账或者少转账。</p>
				<h5>注意事项：汇款时需要注意以下信息，请牢记！</h5>
				<p>户名：北京松雷菲姆勒网络文化传播有限公司</p>
				<p>账户：1100 1008 5000 5300 5677</p>
				<p>开户行：招商银行股份有限公司北京青年路支行</p>
				<p>联行号：1051 0000 4026 （ 非必填项）</p>
				<p>汇付识别码：${orderInfo.ordernum}</p>
				<a class="ph_bg" id="a_qiye_pay">确认</a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
	$(".transfer input,.transfer label").click(function(){
		$(".pay_ok").hide();
		$(".pay_qyzz").show();
	});
	$(".online input,.online label").click(function(){
		$(".pay_ok").show();
		$(".pay_qyzz").hide();
	});
});
</script>  
</body>
</html>
