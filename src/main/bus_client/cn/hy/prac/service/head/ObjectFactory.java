
package cn.hy.prac.service.head;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cn.hy.prac.service.head package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetHeadList_QNAME = new QName("http://head.service.prac.hy.cn/", "getHeadList");
    private final static QName _GetHeadListResponse_QNAME = new QName("http://head.service.prac.hy.cn/", "getHeadListResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cn.hy.prac.service.head
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetHeadList }
     * 
     */
    public GetHeadList createGetHeadList() {
        return new GetHeadList();
    }

    /**
     * Create an instance of {@link GetHeadListResponse }
     * 
     */
    public GetHeadListResponse createGetHeadListResponse() {
        return new GetHeadListResponse();
    }

    /**
     * Create an instance of {@link Head }
     * 
     */
    public Head createHead() {
        return new Head();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHeadList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://head.service.prac.hy.cn/", name = "getHeadList")
    public JAXBElement<GetHeadList> createGetHeadList(GetHeadList value) {
        return new JAXBElement<GetHeadList>(_GetHeadList_QNAME, GetHeadList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHeadListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://head.service.prac.hy.cn/", name = "getHeadListResponse")
    public JAXBElement<GetHeadListResponse> createGetHeadListResponse(GetHeadListResponse value) {
        return new JAXBElement<GetHeadListResponse>(_GetHeadListResponse_QNAME, GetHeadListResponse.class, null, value);
    }

}
