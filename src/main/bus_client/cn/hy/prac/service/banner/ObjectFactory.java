
package cn.hy.prac.service.banner;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cn.hy.prac.service.banner package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetBannerList_QNAME = new QName("http://banner.service.prac.hy.cn/", "getBannerList");
    private final static QName _GetBannerListResponse_QNAME = new QName("http://banner.service.prac.hy.cn/", "getBannerListResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cn.hy.prac.service.banner
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetBannerList }
     * 
     */
    public GetBannerList createGetBannerList() {
        return new GetBannerList();
    }

    /**
     * Create an instance of {@link GetBannerListResponse }
     * 
     */
    public GetBannerListResponse createGetBannerListResponse() {
        return new GetBannerListResponse();
    }

    /**
     * Create an instance of {@link Banner }
     * 
     */
    public Banner createBanner() {
        return new Banner();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBannerList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://banner.service.prac.hy.cn/", name = "getBannerList")
    public JAXBElement<GetBannerList> createGetBannerList(GetBannerList value) {
        return new JAXBElement<GetBannerList>(_GetBannerList_QNAME, GetBannerList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBannerListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://banner.service.prac.hy.cn/", name = "getBannerListResponse")
    public JAXBElement<GetBannerListResponse> createGetBannerListResponse(GetBannerListResponse value) {
        return new JAXBElement<GetBannerListResponse>(_GetBannerListResponse_QNAME, GetBannerListResponse.class, null, value);
    }

}
