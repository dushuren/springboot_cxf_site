package cn.hy.prac.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期处理工具类 
 * 非数据库操作 & 非实例bean操作
 * @author YangJie [2015年11月4日 上午11:43:41]
 */
public class DateUtil {

	
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * 格式化日期
	 * @author YangJie [2016年2月23日 下午3:37:05]
	 * @param date
	 * @return
	 */
	public static String formatDate(Date date){
		return dateFormat.format(date);
	}
	
	/**
	 * 格式化日期+时间
	 * @author YangJie [2016年2月23日 下午3:37:05]
	 * @param date
	 * @return
	 */
	public static String formatDateTime(Date date){
		return dateTimeFormat.format(date);
	}

}