package cn.hy.prac.framework;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.MethodParameter;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@ComponentScan("cn.hy.prac")
public class WebConfig extends WebMvcConfigurerAdapter {
	
	
	@Autowired
	private BootIntercepter bootIntercepter;
	
	@Override // 默认首页
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addRedirectViewController("/", "/crm/user/login");
	}

	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		// TODO Auto-generated method stub
//		registry.
		super.configureViewResolvers(registry);
	}

	@Override // 配置拦截器
	public void addInterceptors(InterceptorRegistry registry) {
		
		registry.addInterceptor(bootIntercepter)
			.addPathPatterns("/**");
		
		super.addInterceptors(registry);
	}
	
	@Override
	public void addReturnValueHandlers(
			List<HandlerMethodReturnValueHandler> returnValueHandlers) {
		// TODO Auto-generated method stub
		HandlerMethodReturnValueHandler handlerMethodReturnValueHandler = new HandlerMethodReturnValueHandler(){

			@Override
			public boolean supportsReturnType(MethodParameter returnType) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void handleReturnValue(Object returnValue,
					MethodParameter returnType,
					ModelAndViewContainer mavContainer,
					NativeWebRequest webRequest) throws Exception {
				// TODO Auto-generated method stub
				System.out.println("123");
//				mavContainer.
			}
			
		};
		returnValueHandlers.add(handlerMethodReturnValueHandler);
		super.addReturnValueHandlers(returnValueHandlers);
	}
	
}
