package cn.hy.prac.framework;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cn.hy.prac.service.banner.BannerService;
import cn.hy.prac.service.head.HeadService;

@Configuration
public class WebserviceClientConfig extends WebserviceSupport {

	@Value("${hyservice.hyWsUrl}")
	private String hyWsUrl; //test_ws路径
	
	@Bean
	public BannerService bannerService() {
		return buildClient(BannerService.class, hyWsUrl + "bus/bannerService");
	}
	
	@Bean
	public HeadService headService() {
		return buildClient(HeadService.class, hyWsUrl + "bus/headService");
	}
	
}
