package cn.hy.prac.controller.head;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.hy.prac.service.head.Head;
import cn.hy.prac.service.head.HeadService;

@Controller
@RequestMapping("/head")
public class HeadComtroller {

	@Autowired
	private HeadService headService;
	
	@RequestMapping("/headList")
	public String headList(Model model){
		
		List<Head> headList = headService.getHeadList();
		model.addAttribute("headList", headList);
		return "/jsps/head.jsp";
	}
}
