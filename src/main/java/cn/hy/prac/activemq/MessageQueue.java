package cn.hy.prac.activemq;

import javax.jms.Queue;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * MQ队列仓库
 * @author Administrator
 *
 */
@Component
public class MessageQueue {

	   //返回一个名为my-message的队列,并且注册为bean
	   @Bean
	   public Queue queue(){
	      return new ActiveMQQueue("my-activemq");
	   }
	
}
